Desktop Client for Bioinformatics
==================================
----
Test bed application for the kmer plugin for ElasticSearch.


Install
-------------

* run **$ mvn clean package** to build.
* java -jar target/DesktopESClient-1.0.0-jar-with-dependencies.jar

Dependencies
-------------
This application requires the following applications/libaries:

* ElasticSearch v0.90.0-RC3 (https://bitbucket.org/dkartaschew/elasticsearch)
* DNA Sequence Library (https://bitbucket.org/dkartaschew/bioinformatics.dnasequence)

Running
-------------
The application uses the ElasticSearch Java API for communicating with the local ElasticSearch Instance. (The cluster configuration should support auto-configuration).

The application supports uploading of GenBank files, and performing searches for kmers as stored within ElasticSearch.
