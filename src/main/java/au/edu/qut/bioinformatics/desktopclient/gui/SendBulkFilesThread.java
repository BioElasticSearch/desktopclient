/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.desktopclient.gui;

import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Send the file to ES in a separate thread.
 */
public class SendBulkFilesThread extends Thread {

    /**
     * The file to send to the server
     */
    private final List<File> files;
    /**
     * The client connection to ES.
     */
    private final ElasticSearchBioInformatics client;

    /**
     * Create a new thread that sends the file to the server.
     *
     * @param client The ES client connection
     * @param file The file to send to ES
     */
    public SendBulkFilesThread(ElasticSearchBioInformatics client, List<File> files) {
        this.client = client;
        this.files = files;
    }

    @Override
    public void run() {
        try {
            List<String> ids = client.sendFilesBulk(files);
            for (final String id : ids)
            {
            	System.out.println("Send passed: " + id);
            }
        } catch (IOException | DNASequenceException ex) {
            Logger.getLogger(SendBulkFilesThread.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Send Failed");
        }
    }
}