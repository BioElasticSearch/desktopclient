/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.elasticsearch;

import au.edu.qut.bioinformatics.desktopclient.ApplicationException;
import au.edu.qut.bioinformatics.dnasequence.DNASequence;
import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;

import static org.elasticsearch.node.NodeBuilder.*;
import static org.elasticsearch.common.xcontent.XContentFactory.*;

import org.elasticsearch.index.query.BaseQueryBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.FuzzyLikeThisFieldQueryBuilder;
import org.elasticsearch.index.query.MoreLikeThisFieldQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SpanNearQueryBuilder;
import org.elasticsearch.index.query.SpanTermQueryBuilder;

/**
 * ElasticSearch connection wrapper. This class operates as a singleton, as this class also initiates a new cluster
 * member to join the local running cluster.
 *
 * @author Darran Kartaschew
 */
public class ElasticSearchBioInformatics {

    /**
     * The field which holds the name of the organism which the DNA sequence is applicable to.
     */
    public static final String FIELD_ORGANISM = "title";
    /**
     * The raw DNA sequence for the organism.
     */
    public static final String FIELD_CONTENT = "content";
    /**
     * The date which the DNA sequence was added to the store.
     */
    public static final String FIELD_DATE_ADDED = "postDate";
    /**
     * The length of the DNA sequence as stored within the store.
     */
    public static final String FIELD_CONTENT_LENGTH = "contentLength";
    /**
     * The filename which the DNA sequence was supplied from.
     */
    public static final String FIELD_FILENAME = "filename";
    /**
     * The store/index which is used to hold the DNA sequences.
     */
    private static final String INDEX_DEFAULT = "documents";
    /**
     * The store type.
     */
    private static final String INDEX_TYPE = "kmer";
    /**
     * Instance of a node. (This wrapper actually runs as a non-storage node within the cluster).
     */
    private final Node node;
    /**
     * Client connection to the local ES node.
     */
    private final Client client;
    /**
     * Instance of this object. Since this object also represents a operating node of a cluster, it is created as a
     * singleton.
     */
    private static ElasticSearchBioInformatics instance = null;

    /**
     * Singleton Constructor for connecting to a local ES cluster.
     */
    private ElasticSearchBioInformatics() {
    	node = nodeBuilder().client(true).node();
        client = node.client();
    }

    /**
     * Get the instance of the client connection to the local ES cluster.
     *
     * @return An instance of this connection.
     */
    public synchronized static ElasticSearchBioInformatics getInstance() {
        if (instance == null) {
            instance = new ElasticSearchBioInformatics();
        }
        return instance;
    }

    /**
     * Close the instance connection to ElasticSearch.
     */
    public void close() {
        if (instance != null) {
        	node.close();
            client.close();
            instance = null;
        }
    }

    /**
     * Upload the requested file into the ElasticSearch Cluster.
     *
     * @param file The file to send to ElastticSearch
     * @return The document ID as stored by ElasticSearch
     * @throws IOException If the file could not be read, or there was an error sending the file to ES.
     */
    public String sendFile(File file) throws IOException, DNASequenceException {

        IndexRequestBuilder request = buildSendFileQuery(file);

        IndexResponse response = request.execute().actionGet();

        return response.getId();
    }
    
    public List<String> sendFilesBulk(List<File> files) throws IOException, DNASequenceException {

    	BulkRequestBuilder bulkRequest = client.prepareBulk();
    	
    	for (final File fileEntry : files) {

	        IndexRequestBuilder request = buildSendFileQuery(fileEntry);
	        bulkRequest.add(request);
	        System.out.println("Added File to task: " + fileEntry.getName());
    	}
    	System.out.println("Sending Files");
    	BulkResponse  response = bulkRequest.execute().actionGet();
    	
    	List<String> ids = new ArrayList<String>();
    	for (final BulkItemResponse ir: response)
    	{
    		ids.add(ir.getId());
    	}
        return ids;
    }
    
    private IndexRequestBuilder buildSendFileQuery(File file) throws IOException, DNASequenceException {

        DNASequence seq = null;
        try {
            seq = new DNASequence(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ElasticSearchBioInformatics.class.getName()).log(Level.SEVERE, null, ex);
            throw new IOException(ex);
        }
        // Send the file to ES

        IndexRequestBuilder request = client.prepareIndex(INDEX_DEFAULT, INDEX_TYPE)
                .setSource(jsonBuilder()
                .startObject()
                .field(FIELD_ORGANISM, seq.getOrganismName())
                .field(FIELD_DATE_ADDED, new Date())
                .field(FIELD_FILENAME, file.getName())
                .field(FIELD_CONTENT_LENGTH, seq.getDNASequenceLength())
                .field(FIELD_CONTENT, new String(seq.getDNASequence()))
                .endObject());

        return request;
    }

    /**
     * Perform a search with a single simple match query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    public SearchResponse searchMatch(String term, boolean explain, boolean termVectors, String termFrequencies) {

        // Break term into singel kmers and create a AND query
        String[] kmers = term.split(" ");
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        for (String mer : kmers) {
            query.must(QueryBuilders.matchQuery(FIELD_CONTENT, mer));
        }
        // Perform the query.
        return doQuery(query, explain, termVectors, termFrequencies);
    }

    /**
     * Perform a search with a simple span query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     * @throws ApplicationException If the terms term does not consist of 3 parts, 2x terms and a maximum distance.
     */
    public SearchResponse searchSpan(String term, boolean explain, boolean termVectors, String termFrequencies) throws ApplicationException {

        String[] subterms = term.split(",");

        if (subterms.length == 0) {
            throw new ApplicationException("The query should consist of at least span query");
        }
        if (subterms.length == 1) {
            return doQuery(buildSpanQuery(term), explain, termVectors, termFrequencies);
        } else {
            BoolQueryBuilder query = QueryBuilders.boolQuery();
            for (String subterm : subterms) {
                query.must(buildSpanQuery(subterm));
            }
            // Perform the query.
            return doQuery(query, explain, termVectors, termFrequencies);
        }
    }

    /**
     * Create a span query based on the term passed.
     *
     * @param term A search tuple that contains 2 terms and a distance.
     * @return A SpanQuery.
     * @throws ApplicationException An exception is thrown if the search term in not in the correct format.
     */
    private SpanNearQueryBuilder buildSpanQuery(String term) throws ApplicationException {
        // Break term into singel kmers and create a AND query
        String[] kmers = term.split(" ");
        if (kmers.length != 3) {
            throw new ApplicationException("The query should consist of two terms and a distance (integer)");
        }

        // Attempt to get the span distance.
        int spanDistance;
        try {
            spanDistance = Integer.parseInt(kmers[2]);
        } catch (NumberFormatException ex) {
            throw new ApplicationException("The query should consist of two terms and a distance (integer)");
        }

        // Span Near Query.
        return QueryBuilders
                .spanNearQuery()
                .clause(new SpanTermQueryBuilder(FIELD_CONTENT, kmers[0]))
                .clause(new SpanTermQueryBuilder(FIELD_CONTENT, kmers[1]))
                .slop(spanDistance)
                .inOrder(false);
    }

    /**
     * Perform a search with a single More Like This query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    public SearchResponse searchSimilar(String term, boolean explain, boolean termVectors, String termFrequencies) {

        // Break term into singel kmers and create a AND query
        MoreLikeThisFieldQueryBuilder query = QueryBuilders.moreLikeThisFieldQuery(FIELD_CONTENT)
                .analyzer("kmer")
                .percentTermsToMatch(0.1f)
                .minTermFreq(0)
                .minDocFreq(0)
                .likeText(term);

        // Perform the query.
        return doQuery(query, explain, false, null); // We never return term vectors for a similarity search.
    }

    /**
     * Perform a search with a single Fuzzy Like This query against ElasticSearch
     *
     * @param term The term to search for
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies True, if the frequencies for ALL terms in the document are to be returned.
     * @return A SearchRespons from ElasticSearch
     */
    public SearchResponse searchSimilarFuzzy(String term, boolean explain, boolean termVectors, String termFrequencies) {

        // Break term into singel kmers and create a AND query
        FuzzyLikeThisFieldQueryBuilder query = QueryBuilders.fuzzyLikeThisFieldQuery(FIELD_CONTENT)
                .analyzer("kmer")
                .ignoreTF(true)
                //.minSimilarity(0.5f)
                .likeText(term);

        // Perform the query.
        return doQuery(query, explain, false, null); // We never return term vectors for a similarity search.
    }

    /**
     * Perform the query using the specified query and parameters.
     *
     * @param query The query which has been built using one of the query builders.
     * @param explain True if the query should return an explanation for the query results.
     * @param termVectors True if the query should attempt to return the vectors for terms found.
     * @param termFrequencies "terms" if frequency values for search terms is to be returned, or the minimum number of terms. (null to disable).
     * @return A SearchRespons from ElasticSearch
     */
    private SearchResponse doQuery(BaseQueryBuilder query, boolean explain, boolean termVectors, String termFrequencies) {
        
        SearchResponse response = client.prepareSearch(INDEX_DEFAULT)
                .setSearchType(SearchType.DEFAULT)
                .addFields(FIELD_ORGANISM, FIELD_FILENAME, FIELD_CONTENT_LENGTH, FIELD_DATE_ADDED)
                .setQuery(query)
                .setExplain(explain)
                .setTerms(termVectors)
                .setTermFrequencies(termFrequencies)
                .setSize(10)
                .execute()
                .actionGet();

        return response;
    }
}
